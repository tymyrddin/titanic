# Titanic R&D ML pipeline

  1. Exploratory Data Analysis (EDA)
  2. Feature Engineering
  3. Feature Selection
  4. Model Training
  5. Obtaining Predictions / Scoring

