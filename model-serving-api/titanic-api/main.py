import fastapi

app = FastAPI()


@app.get("/")
async def read_root():
    return {"message": "Hello World"}


@app.get("/square")
async def square(num: int):
    result = num**2
    return {"squared": result}
