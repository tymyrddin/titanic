# Production Titanic Classification Model Package

## Run With Tox (Recommended)
- Download the data from: https://www.openml.org/data/get_csv/16826755/phpMYEkMl
- Save the file as `raw.csv` in the classification_model/datasets directory
- `pip install tox`
- Make sure you are in the production-model-package directory (where the tox.ini file is) then run the command: `tox` (this runs the tests and typechecks, trains the model under the hood). The first time you run this it creates a virtual env and installs
dependencies, so takes a few minutes.

## Run Without Tox
- Download the data from: https://www.openml.org/data/get_csv/16826755/phpMYEkMl
- Save the file as `raw.csv` in the classification_model/datasets directory
- Add production-model-package *and* classification_model paths to your system PYTHONPATH (export PYTHONPATH=$PYTHONPATH:/<path_to_modules>)
- `pip install -r requirements/test_requirements`
- Train the model: `python classification_model/train_pipeline.py`
- Run the tests `pytest tests`

## Resources

- [PEP8](https://www.python.org/dev/peps/pep-0008/)
- [PEP 484 - typehints](https://www.python.org/dev/peps/pep-0484/)
- [Using requirements files](https://realpython.com/lessons/using-requirement-files/)
- [Tox Overview](https://christophergs.com/python/2020/04/12/python-tox-why-use-it-and-tutorial/)
- [Building Python Packages](https://packaging.python.org/tutorials/packaging-projects/)
- [Why not use Python for config](https://hitchdev.com/strictyaml/why-not/turing-complete-code/)
- [Primer on pyproject.toml](https://snarky.ca/what-the-heck-is-pyproject-toml/)